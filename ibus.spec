%global with_pkg_config %(pkg-config --version >/dev/null 2>&1 && echo -n "1" || echo -n "0")
%global with_kde5 1
%global ibus_api_version 1.0
%global __python %{__python3}
%global dbus_python_version 0.83.0

Name:                   ibus
Version:                1.5.30
Release:                2
Summary:                Intelligent Input Bus for Linux OS
License:                LGPL-2.1-or-later
URL:                    https://github.com/ibus/%name/wiki
Source0:                https://github.com/ibus/ibus/releases/download/%{version}/%{name}-%{version}.tar.gz
#Source1,2 come form fedora
Source1:                %{name}-xinput
Source2:                %{name}.conf.5
# rpm fileattrs for downstream pacakges so ibus dependency could be pulled in automatically
Source3:                %{name}.attr
Patch1:                 %{name}-1385349-segv-bus-proxy.patch
Patch2:                 30a3641e19c541924959a5770dd784b4424288d4.patch

BuildRequires:          gettext-devel libtool glib2-doc gtk2-devel gtk3-devel dbus-glib-devel gtk-doc dconf-devel dbus-x11 python3-devel
BuildRequires:          dbus-python-devel >= %{dbus_python_version} desktop-file-utils python3-gobject vala vala-devel vala-tools
BuildRequires:          iso-codes-devel libnotify-devel libwayland-client-devel qt5-qtbase-devel cldr-emoji-annotation
BuildRequires:          unicode-emoji unicode-ucd libXtst-devel libxslt gobject-introspection-devel pygobject3-devel
BuildRequires:          libdbusmenu-gtk3-devel gtk4-devel

Requires:               iso-codes dbus-x11 dconf python3-gobject python3
Requires:               xorg-x11-xinit setxkbmap

Requires:               desktop-file-utils glib2
Requires(post):         desktop-file-utils glib2 
Requires(postun):       desktop-file-utils
Requires:               dconf
Requires(postun):       dconf
Requires(posttrans):    dconf
Requires:               %{_sbindir}/alternatives
Requires(post):         %{_sbindir}/alternatives
Requires(postun):       %{_sbindir}/alternatives

Provides:               ibus-gtk = %{version}-%{release}
Obsoletes:              ibus-gtk < %{version}-%{release}
Provides:               ibus-gtk2 = %{version}-%{release}  ibus-gtk3 = %{version}-%{release}  ibus-setup = %{version}-%{release}  ibus-wayland = %{version}-%{release}
Obsoletes:              ibus-gtk2 < %{version}-%{release}  ibus-gtk3 < %{version}-%{release}  ibus-setup < %{version}-%{release}  ibus-wayland < %{version}-%{release}

%global _xinputconf %{_sysconfdir}/X11/xinit/xinput.d/ibus.conf

%description
IBus means Intelligent Input Bus. It is an input framework for Linux OS.

%package libs
Summary:        IBus libraries
Requires:       dbus >= 1.2.4
Requires:       glib2 gobject-introspection

%description libs
This package contains the libraries for IBus

%package devel
Summary:                Development tools for ibus
Requires:               %{name} = %{version}-%{release}
Requires:               dbus-devel glib2-devel gobject-introspection-devel vala
Provides:               ibus-devel-docs = %{version}-%{release}
Obsoletes:              ibus-devel-docs < %{version}-%{release}

%package_help

%description devel
The ibus-devel package contains the header files and developer
docs for ibus.

%prep
%autosetup -n %{name}-%{version} -p1
cp client/gtk2/ibusimcontext.c client/gtk3/ibusimcontext.c || :

diff client/gtk2/ibusimcontext.c client/gtk3/ibusimcontext.c
if test $? -ne 0 ; then
    echo "Have to copy ibusimcontext.c into client/gtk3"
    abort
fi

%build
autoreconf -ivf
%configure --disable-static --enable-gtk2 --enable-gtk3 --enable-xim --enable-gtk-doc --enable-surrounding-text \
           --with-python=python3 --disable-python2 --enable-wayland --enable-introspection %{nil}

%make_build -C ui/gtk3 maintainer-clean-generic
%make_build

%install
%make_install INSTALL='install -p'
%delete_la

install -pm 644 -D %{S:2} $RPM_BUILD_ROOT%{_datadir}/man/man5/

install -pm 644 -D %{S:1} $RPM_BUILD_ROOT%{_xinputconf}

install -pm 644 -D %{S:3} $RPM_BUILD_ROOT%{_fileattrsdir}/%{name}.attr

echo "NoDisplay=true" >> $RPM_BUILD_ROOT%{_datadir}/applications/org.freedesktop.IBus.Setup.desktop

%find_lang %{name}10

%check
%make_build check DISABLE_GUI_TESTS="ibus-compose ibus-keypress test-stress xkb-latin-layouts" VERBOSE=1 %{nil}

%post
%{_sbindir}/alternatives --install %{_sysconfdir}/X11/xinit/xinputrc xinputrc %{_xinputconf} 83 || :

%postun
if [ "$1" -eq 0 ]; then
  %{_sbindir}/alternatives --remove xinputrc %{_xinputconf} || :
  [ -L %{_sysconfdir}/alternatives/xinputrc -a "`readlink %{_sysconfdir}/alternatives/xinputrc`" = "%{_xinputconf}" ] && %{_sbindir}/alternatives --auto xinputrc || :

  dconf update || :
  [ -f %{_sysconfdir}/dconf/db/ibus ] && \
      rm %{_sysconfdir}/dconf/db/ibus || :
  [ -f /var/cache/ibus/bus/registry ] && \
      rm /var/cache/ibus/bus/registry || :
fi

%posttrans
dconf update || :

%transfiletriggerin -- %{_datadir}/ibus/component
[ -x %{_bindir}/ibus ] && \
  %{_bindir}/ibus write-cache --system &>/dev/null || :
 
%transfiletriggerpostun -- %{_datadir}/ibus/component
[ -x %{_bindir}/ibus ] && \
  %{_bindir}/ibus write-cache --system &>/dev/null || :

%files -f %{name}10.lang
%license COPYING COPYING.unicode
%doc AUTHORS
%{_bindir}/ibus
%{_bindir}/ibus-daemon
%{_bindir}/ibus-setup
%{_datadir}/applications/*.desktop
%{_datadir}/bash-completion/completions/ibus.bash
%{_datadir}/dbus-1/services/*.service
%{_datadir}/GConf/gsettings/*
%{_datadir}/glib-2.0/schemas/*.xml
%{_datadir}/ibus
%{_datadir}/icons/hicolor/*/apps/*
%{_libexecdir}/*
%{_sysconfdir}/dconf/db/ibus.d
%{_sysconfdir}/dconf/profile/ibus
%dir %{_sysconfdir}/xdg/Xwayland-session.d
%{_sysconfdir}/xdg/Xwayland-session.d/10-ibus-x11
%{_prefix}/lib/systemd/user/gnome-session.target.wants/*.service
%{_prefix}/lib/systemd/user/org.freedesktop.IBus.session.*.service
%python3_sitearch/gi/overrides/__pycache__/*.py*
%python3_sitearch/gi/overrides/IBus.py
%dir %{_sysconfdir}/X11/xinit/xinput.d
%config %{_xinputconf}
%{_libdir}/gtk-2.0/*
%{_libdir}/gtk-3.0/*
%{_libdir}/gtk-4.0/*

%files libs
%{_libdir}/libibus-*%{ibus_api_version}.so.*
%{_libdir}/girepository-1.0/IBus*-1.0.typelib

%files devel
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
%{_includedir}/*
%{_datadir}/gettext/its/ibus.*
%{_datadir}/gir-1.0/IBus*-1.0.gir
%{_datadir}/vala/vapi/ibus-*1.0.vapi
%{_datadir}/vala/vapi/ibus-*1.0.deps
%{_fileattrsdir}/%{name}.attr

%files help
%doc README
%{_mandir}/*/*
%{_datadir}/gtk-doc/html/*

%changelog
* Tue Dec 17 2024 Funda Wang <fundawang@yeah.net> - 1.5.30-2
- add rpm file attr so that ibus dependency could be pulled in automatically

* Tue Aug 20 2024 Funda Wang <fundawang@yeah.net> - 1.5.30-1
- update to 1.5.30
- add file triggers for ibus components

* Wed Mar 13 2024 herengui <herengui@kylinsec.com.cn> - 1.5.29-2
- add buildrequires gtk4-devel since enable_gtk4 is set to yes by default.

* Thu Feb 01 2024 zhouwenpei <zhouwenpei1@h-partners.com> - 1.5.29-1
- update to 1.5.29

* Fri Sep 15 2023 yangxianzhao <yangxianzhao@uniontech.com> - 1.5.23-3
- fix ibus setup failure

* Mon Apr 18 2022 Jun Yang <jun.yang@suse.com> - 1.5.23-2
- remove self-dependency

* Fri Jan 29 2021 zhanzhimin <zhanzhimin@huawei.com> - 1.5.23-1
- update to 1.5.23

* 20201121063007667187 patch-tracking 1.5.22-3
- append patch file of upstream repository from <02338ce751a1ed5b9b892fba530ec2fe211d314e> to <b72efea42d5f72e08e2774ae03027c246d41cab7>

* Tue Sep 8 2020 hanhui <hanhui15@huawei.com> - 1.5.22-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify source url

* Wed Jun 10 2020 zhujunhao <zhujunhao8@huawei.com> - 1.5.22-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update to 1.5.22

* Wed Feb 26 2020 hexiujun <hexiujun1@huawei.com> - 1.5.19-7
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:deprecated python2

* Wed Oct 09 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.5.19-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add COPYING.unicode 

* Thu Sep 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.5.19-5
- Package init
